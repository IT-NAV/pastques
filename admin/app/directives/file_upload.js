/**
 * Created by Che Henry on 7/20/2016.
 */

app.directive('fileInput', function($parse){
    return{
        restrict : 'A',
        link: function(scope, elem, attrs){
            elem.bind('change', function(){
                $parse(attrs.fileInput).assign(scope, elem[0].files);
                scope.$apply();
            });
        }
    }
});