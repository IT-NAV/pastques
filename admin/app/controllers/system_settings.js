/**
 * Created by Che Henry on 7/20/2016.
 */


'use strict';

app.controller("SystemSettings", function($scope,$http,Data,$route){
    $scope.displaySystemInfo = function(){
        Data.post('settings.php',{'action':'all'}).success(function(data){
            for(var i=0; i<data.length; i++){
                $scope.id = data[i].idsetting;
                $scope.company_name = data[i].company_name;
                $scope.phone = data[i].phone;
                $scope.email = data[i].email;
                $scope.location = data[i].location;
            }
        });
    };
    $scope.displaySystemInfo();

    $scope.update = function(){
        Data.post('settings.php',{
            'action':'update',
            'company_name':$scope.company_name,
            'phone':$scope.phone,
            'email':$scope.email,
            'location':$scope.location
        }).success(function(data){
            if(data.indexOf("successful") > -1){
                $route.reload();
                $scope.displaySystemInfo();
                Data.toast('success',"Data updated successfully")
            }else{
                Data.toast('error',"Oops! Error while updating data!")
            }
        })
    }

});