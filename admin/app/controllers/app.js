/**
 * Created by Che Henry on 7/6/2016.
 */

'use strict';

var app = angular.module('PastQuest', ['ngRoute','ngAnimate','toaster','datatables']);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'templates/dashboard.html'
            })
            .when('/new_question', {
                templateUrl: 'templates/new-question.html'
            })
            .when('/login', {
                templateUrl: 'templates/login.html',
                controller: 'Login'
            })
            .when('/view_questions', {
                templateUrl: 'templates/view_questions.html'
            })
            .when('/view_depart', {
                templateUrl: 'templates/view_depart.html'
            })
            .when('/new_depart', {
                templateUrl: 'templates/new_depart.html'
            })
            .when('/new_faculty', {
                templateUrl: 'templates/new_faculty.html'
            })
            .when('/view_faculties', {
                templateUrl: 'templates/view_faculty.html'
            })
            .when('/new_course', {
                templateUrl: 'templates/new_course.html'
            })
            .when('/view_course', {
                templateUrl: 'templates/view_courses.html'
            })

            .when('/admin_settings', {
                templateUrl: 'templates/admin_setting.html'
                //controller: 'NewProperty'
            })
            .when('/system_settings', {
                templateUrl: 'templates/system_setting.html'
                //controller: 'SystemSettings'
            })
            .when('/question_settings', {
                templateUrl: 'templates/question_setting.html'
                //controller: 'NewProperty'
            })

            .when('/view_users', {
                templateUrl: 'templates/inbox_view.html'
                //controller: 'NewProperty'
            })
            .otherwise({
                redirectTo: '/dashboard'
            });
    }]);

app.run(function($rootScope, $location, loginService){
    var routesPermissions = ['/','/new_question','/view_questions','/view_depart','/new_depart','/new_faculty','/view_faculties',
        '/new_course','/view_course','/admin_settings','/system_settings','/question_settings','/view_users'];

    $rootScope.$on('$routeChangeStart', function(){
        if(routesPermissions.indexOf($location.path()) != -1 ){

            var connected = loginService.islogged();
            connected.success(function(data){
                if(!data)
                    $location.path('/login');
            });

        }
    })
});

