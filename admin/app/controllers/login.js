/**
 * Created by Che Henry on 8/1/2016.
 */

app.controller('Login',function($scope,loginService){
    $scope.header = "Sign in to start your session!";

    $scope.login = function(){
        loginService.login($scope.username,$scope.pass,$scope);
    };
});

