/**
 * Created by Che Henry on 8/4/2016.
 */

'use strict';

app.controller('Default',function($scope,$http,loginService){
   $scope.checkSession = function(){
       loginService.sessionParams($scope);
   };

    $scope.logout = function(){
        loginService.logout($scope);
    };

    $scope.checkSession();
});