<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 8/1/2016
 * Time: 8:06 PM
 */
require_once('../../../api/server/dbConnect.php');
require_once('../../../api/server/session.php');
$obj = new dbConnect();
$sess = new session();
$conn = $obj->connect();
$data = json_decode(file_get_contents("php://input"));
$action = mysqli_real_escape_string($conn,$data->action);

switch($action){
    case 'login':
        $username = mysqli_real_escape_string($conn,$data->username);
        $pass = mysqli_real_escape_string($conn,$data->pass);

        if($username == "admin" && $pass == "admin"){
            session_start();
            $sess->start_session(uniqid('u_id'),$username);

            print("Login successfully made!");
        }else{
            print("Wrong user credentials!");
        }
    break;

    case 'logout':
        session_start();
        if($sess->destroySession()){
            print("logged out successful");
        }else{
            print("Oops! Unable to logout!");
        }
    break;

    case 'checkSession':
        session_start();
        if(isset($_SESSION['username'])) print 'authenticated';
    break;

    case 'sessionParams':
        $params = $sess->getSession();
        print(json_encode($params));
        break;
}