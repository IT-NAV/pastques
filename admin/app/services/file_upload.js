/**
 * Created by Che Henry on 7/20/2016.
 */

app.factory("FileUpload",
    function () {
        var obj = {};
        obj.retrieveFiles = function(scope){
            var fd = new FormData();
            angular.forEach(scope.files, function(file){
                fd.append('file',file);
                });
            return fd;
        };

        obj.getSingleFile = function(){
            var file = "";
            $('input[type=file]').change(function(event){
                file = event.target.files[0].name;
            });
            return file;
        };

        obj.getMultipleFiles = function(){
            var files = {};
            $('input[type=file]').change(function(event){
                for(var i=0; i<event.length; i++){
                    files.append('file'+i,event.target.files[i].name);
                }
            });
            return files;
        };
    }
);

