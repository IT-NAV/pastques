/**
 * Created by Che Henry on 8/4/2016.
 */

app.controller('Search', function($scope,DataProvider,Data,$location){

    $scope.search_results = function(){
        var query = localStorage.getItem('query');
        if(query)
            Data.post('user_search.php',{'query':query}).success(function(data){
                if(data){
                    $scope.results = data;
                    $scope.msg = "data";
                }else{$scope.msg = "no data"}
            });
        else Data.toast("error","Please enter a search query!");
    };

    $scope.clearStorage = function(){
        localStorage.removeItem('query');
        $location.path('/');
    };

    $scope.addItemToFolder = function(id){
        Data.post('my_folder.php',{'id':id,'query':'add_item'}).success(function(data){
            if(data.indexOf('successful') > -1){
                Data.toast('success','The file was added to your folder');
            }else{
                Data.toast('error',data);
            }
        })
    };

    $scope.search_results();
});