/**
 * Created by Che Henry on 8/4/2016.
 */

app.controller('Main', function($scope,$http,Data,DataProvider,$location){
    $scope.search = function(){
        if($scope.search_query) {
            localStorage.setItem('query', $scope.search_query);
            $location.path('/search-results');
        }
        else Data.toast("error","Please enter a search query!");
    }
});