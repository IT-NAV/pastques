/**
 * Created by Che Henry on 8/4/2016.
 */

app.controller('MyFolder', function($scope,DataProvider,Data,$route){

    $scope.getFolderItems = function(){
        Data.post('my_folder.php',{'query':'get_items'}).success(function(data){
            $scope.results = data;
        });
    };
    $scope.getFolderItems();

    $scope.removeItem = function(id){
        Data.post('my_folder.php',{'id':id,'query':'remove_item'}).success(function(data){
            if(data.indexOf('successful') > -1){
                $route.reload();
            }
        })
    }
});