/**
 * Created by Che Henry on 7/6/2016.
 */

'use strict';

var app = angular.module('PastQues', ['ngRoute','ngAnimate','toaster','datatables']);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'client/templates/home.html',
                controller: 'Main'
            })
            .when('/search-results', {
                templateUrl: 'client/templates/search_results.html',
                controller: 'Search'
            })
            .when('/my-folder', {
                templateUrl: 'client/templates/my_folder.html',
                controller: 'MyFolder'
            })
            .when('/login', {
                templateUrl: 'client/templates/login.html',
                controller: 'Authentication'
            })
            .when('/register', {
                templateUrl: 'client/templates/register.html',
                controller: 'Authentication'
            })
            .otherwise({
                redirectTo: '/'
            });
    }
]);

app.run(function($rootScope, $location, loginService){
    var routesPermissions = ['/my-folder'];

    $rootScope.$on('$routeChangeStart', function(){
        if(routesPermissions.indexOf($location.path()) != -1 ){

            var connected = loginService.islogged('client/app/server/auth.php');
            connected.success(function(data){
                if(!data)
                    $location.path('/login');
            });

        }
    })
});

