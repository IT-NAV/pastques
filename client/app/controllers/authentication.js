/**
 * Created by Che Henry on 8/1/2016.
 */

app.controller('Authentication',function($scope,$location,loginService,Data){
    $scope.login = function(){
        if($scope.username && $scope.phone){
            loginService.login('client/app/server/auth.php',
                {'username':$scope.username,'phone':$scope.phone,'action':'login'},$scope,'/my-folder'
            );
        }else{
            Data.toast('warning','Please all fields are required');
        }
    };

    $scope.register = function(){
        if($scope.username && $scope.phone && $scope.email){
            loginService.login('client/app/server/auth.php',
                {'username':$scope.username,'phone':$scope.phone,'email':$scope.email,'action':'register'},$scope,'/'
            );
        }else{
            Data.toast('warning','Please all fields are required');
        }
    };

    $scope.checkSession = function(){
        loginService.sessionParams($scope);
    };
});

