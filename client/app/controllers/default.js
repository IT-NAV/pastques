/**
 * Created by Che Henry on 8/4/2016.
 */

'use strict';

app.controller('Default',function($scope,$http,loginService){
   $scope.checkSession = function(){
       loginService.sessionParams('client/app/server/auth.php',$scope);
   };

    $scope.logout = function(){
        loginService.logout('client/app/server/auth.php',$scope);
    };

    $scope.checkSession();
});