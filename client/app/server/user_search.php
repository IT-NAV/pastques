<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 8/4/2016
 * Time: 7:39 PM
 */

require_once('../../../api/server/Search.php');
require_once('../../../api/server/dbConnect.php');
require_once('../../../api/server/dbHandle.php');
$obj = new Search();
$dbConn = new dbConnect();
$dbHan = new dbHandle();
$conn = $dbConn->connect();

$data = json_decode(file_get_contents("php://input"));
$query = mysqli_real_escape_string($conn,$data->query);

$key_words = $obj->split_text($query);
//print(json_encode($key_words));
$results = [];
for($i=0; $i<sizeof($query); $i++){
    $sql = "SELECT id_ques,file,fac,dept,cours,code,sess,exam_year FROM questions WHERE fac LIKE '%$key_words[$i]%' OR dept LIKE '%$key_words[$i]%' OR cours LIKE '%$key_words[$i]%'
            OR code LIKE '%$key_words[$i]%' OR sess LIKE '%$key_words[$i]%' OR exam_year LIKE '%$key_words[$i]%'";
    $results = $dbHan->db_select($conn,$sql);
}
print(json_encode($results));