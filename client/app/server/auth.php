<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 8/1/2016
 * Time: 8:06 PM
 */
require_once('../../../api/server/dbConnect.php');
require_once('../../../api/server/dbHandle.php');
require_once('../../../api/server/session.php');
$obj = new dbConnect();
$res = new dbHandle();
$sess = new session();
$conn = $obj->connect();
$data = json_decode(file_get_contents("php://input"));
$action = mysqli_real_escape_string($conn,$data->action);

switch($action){
    case 'login':
        $username = mysqli_real_escape_string($conn,$data->username);
        $phone = mysqli_real_escape_string($conn,$data->phone);
        $sql = "SELECT username,phone FROM users WHERE username = '$username' AND phone = '$phone'";
        $result = $res->db_select_with_raw_result($conn,$sql);
        if($result){
            if($username == $result['username'] && $phone == $result['phone']){
                session_start();
                $sess->start_session(uniqid('u_id'),$username);
                $_SESSION['phone'] = $phone;
                print("Login successfully made!");
            }else{
                print("Wrong user credentials!");
            }
        }else{print("Wrong user credentials!");}
    break;

    case 'register':
        $username = mysqli_real_escape_string($conn,$data->username);
        $phone = mysqli_real_escape_string($conn,$data->phone);
        $email = mysqli_real_escape_string($conn,$data->email);
        $sql = "INSERT INTO users(username,phone,email) VALUES ('$username','$phone','$email')";

        $result = $res->single_insert($conn,$sql);
        if($result){
            session_start();
            $sess->start_session(uniqid('u_id'),$username);
            $_SESSION['phone'] = $phone;
            print("User successfully registered");
        }else{print("Unable to register user");}
    break;

    case 'logout':
        session_start();
        if($sess->destroySession()){
            unset($_SESSION['phone']);
            print("logged out successful");
        }else{
            print("Oops! Unable to logout!");
        }
    break;

    case 'checkSession':
        session_start();
        if(isset($_SESSION['username']) && isset($_SESSION['phone'])) print 'authenticated';
    break;

    case 'sessionParams':
        $params = $sess->getSession();
        print(json_encode($params));
        break;
}