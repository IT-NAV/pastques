<?php
/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 8/4/2016
 * Time: 7:39 PM
 */

require_once('../../../api/server/dbConnect.php');
require_once('../../../api/server/session.php');
require_once('../../../api/server/dbHandle.php');
require_once('../../../api/server/response.php');
$dbConn = new dbConnect();
$sess = new session();
$dbHan = new dbHandle();
$ress = new response();
$conn = $dbConn->connect();

$data = json_decode(file_get_contents("php://input"));
$query = mysqli_real_escape_string($conn,$data->query);

switch($query){
    case 'add_item':
        $id = mysqli_real_escape_string($conn,$data->id);
        $ses_params = $sess->getSession();
        $username = $ses_params['username'];

        $query = $dbHan->db_select_with_raw_result($conn,"SELECT iduser FROM users WHERE username = '$username'");
        $userId = $query['iduser'];
        $sql = "INSERT INTO my_folder (id_file,id_user) VALUES ($id,$userId)";
        $ress->message_with_error(($dbHan->single_insert($conn,$sql)),"Item added successfully","Oops! Error while adding item");
    break;

    case 'get_items':
        $ses_params = $sess->getSession();
        $username = $ses_params['username'];

        $sql = "SELECT DISTINCT(file),id_item,date_added FROM questions INNER JOIN my_folder ON id_ques = id_file
                AND my_folder.id_user = (SELECT iduser FROM users WHERE username = '$username') ORDER BY date_added";
        if($data = $dbHan->db_select($conn,$sql)){
            print(json_encode($data));
        }
    break;

    case 'remove_item':
        $id = mysqli_real_escape_string($conn,$data->id);
        $sql = "DELETE FROM my_folder WHERE id_item = $id";
        $dbHan->db_insert_update_delete($conn,$sql,"Item removed successfully","Oops! Unable to remove item");

}