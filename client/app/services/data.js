

app.factory("Data", ['$http', 'toaster',
    function ($http, toaster) {

        var basePath = 'client/app/server/';

        var obj = {};
        obj.toast = function (type,data) {
            toaster.pop(type, data);
        };
        obj.get = function (q) {
            return $http.get(basePath + q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(basePath + q, object).success(function (data,status) {
                return {data:data,status:status};
            });
        };
        obj.put = function (q, object) {
            return $http.put(basePath + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(basePath + q).then(function (results) {
                return results.data;
            });
        };
        obj.upload = function (q, object,transformRequest,header) {
            return $http({
                method: 'POST',
                url: basePath + q,
                data: object,
                transformRequest: transformRequest,
                headers: header
            }).then(function (results) {
                    return results.data;
                });
        };
        //function used to get an id;
        obj.getId = function(id){
            return id;
        };
        obj.genRef = function(prefix){
            return prefix+Math.random();
        };

        return obj;
}]);