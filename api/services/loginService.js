/**
 * Created by Che Henry on 8/1/2016.
 */
'use strict';

app.factory('loginService', function($location,$http,$route,Data){
    return{
        login : function(path,obj,scope,redirect_to){
            $http.post(path,obj).success(function(data){
                    if(data.indexOf('successful') > -1){
                        sessionStorage.setItem('user',obj.username);
                        $location.path(redirect_to);
                        Data.toast('success',"Login Successful!");
                    }else{
                        Data.toast('error',"Incorrect Credentials!");
                        console.log(data);
                        scope.header = data;
                    }
                })
            },

        register : function(path,obj,scope,redirect_to){
            $http.post(path,obj).success(function(data){
                if(data.indexOf('successful') > -1){
                    sessionStorage.setItem('user',obj.username);
                    $location.path(redirect_to);
                }else{
                    $route.reload();
                    Data.toast("error","Sorry there was an error registering you!");
                    scope.header = data;
                }
            })
        },

        logout : function(path,scope){
            $http.post(path,{'action':'logout'}).success(function(data){
                    if(data.indexOf('successful') > -1){
                        sessionStorage.removeItem('user');
                        $location.path('/login');
                    }else{
                        scope.header = data;
                    }
                })
            },

        sessionParams : function(path,scope){
            $http.post(path,{'action':'sessionParams'}).success(function(data){
                scope.session = data;
            })
        },

        islogged : function(path){
            var $checkSessionServer = $http.post(path,{'action':'checkSession'});
            return $checkSessionServer;
        }
    }
});