/**
 * Created by Che Henry on 7/4/2016.
 */

'use strict';

app.factory('DataProvider', function() {
    var contest_id = {};
    function set(data) {
        contest_id = data;
    }
    function get() {
        return contest_id;
    }

    return {
        set: set,
        get: get
    }

});