<?php

/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 8/3/2016
 * Time: 3:38 PM
 */
class session {
    private $sess = array();
    private $msg;

    function __construct(){

    }

    public function getSession(){
        if (!isset($_SESSION)) {
            session_start();
        }
        if(isset($_SESSION['uid']))
        {
            $this->sess["uid"] = $_SESSION['uid'];
            $this->sess["username"] = $_SESSION['username'];
        }
        else
        {
            $this->sess["uid"] = '';
            $this->sess["username"] = 'Guest';
        }
        return $this->sess;
    }

    public function destroySession(){
        if (!isset($_SESSION)) {
            session_start();
        }
        if(isSet($_SESSION['uid']))
        {
            unset($_SESSION['uid']);
            unset($_SESSION['username']);

            $info='info';
            if(isSet($_COOKIE[$info]))
            {
                setcookie ($info, '', time() - $cookie_time);
            }
            $this->msg="Logged Out Successfully...";
        }
        else
        {
            $this->msg = "Not logged in...";
        }
        return $this->msg;
    }

    public function start_session($uid,$username){
        session_start();
        $_SESSION['uid'] = $uid;
        $_SESSION['username'] = $username;
    }

}