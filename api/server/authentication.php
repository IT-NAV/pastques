<?php

/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 8/3/2016
 * Time: 1:51 PM
 */
class authentication
{

    function __construct(){

    }

    public function signin($username,$password){
        require_once'dbConnect.php';
        require_once'dbHandle.php';
        require_once'session.php';
        require_once 'passwordHash.php';
        $response = array();
        $connection = new dbConnect();
        $sess = new session();
        $conn = $connection->connect();
        $db = new dbHandle();
        $user = $db->db_select($conn,"select uid,username,password from user_account_admin where username='$username'");
        if ($user != NULL) {
            if(passwordHash::check_password($user['password'],$password)){
                $response['status'] = "success";
                $response['message'] = 'Logged in successfully.';
                $response['username'] = $user['username'];
                $response['uid'] = $user['uid'];
                if (!isset($_SESSION)) {
                    session_start();
                }
                $sess->start_session($user['uid'],$user['username']);
            } else {
                $response['status'] = "error";
                $response['message'] = 'Login failed. Incorrect credentials';
            }
        }else {
            $response['status'] = "error";
            $response['message'] = 'No such user is registered';
        }
        $response["code"] = 200;
        print(json_encode($response));
    }

    public function signup($tabble_name,$username,$email,$password){
        $response = array();
        $r = json_decode(file_get_contents("php://input"));
        require_once 'passwordHash.php';
        $connection = new dbConnect();
        $conn = $connection->connect();
        $db = new dbHandle();
        $isUserExists = $db->db_select($conn,"select 1 from user_account where username='$username' or email='$email'");
        if(!$isUserExists){
            $r->password = passwordHash::hash($password);
            $column_names = array('username', 'email', 'password');
            $result = $db->single_insert($conn,"INSERT INTO '$tabble_name' (username,email,password VALUES ('$username','$email','$password'))");
            if ($result != NULL) {
                $response["status"] = "success";
                $response["message"] = "User account created successfully";
                $response["uid"] = $result;
                if (!isset($_SESSION)) {
                    session_start();
                }
                $_SESSION['uid'] = $response["uid"];
                $_SESSION['username'] = $username;
                $_SESSION['email'] = $email;
                $response["code"] = 200;
                print($response);
            } else {
                $response["status"] = "error";
                $response["message"] = "Failed to create customer. Please try again";
                $response["code"] = 201;
                print($response);
            }
        }else{
            $response["status"] = "error";
            $response["message"] = "An user with the provided phone or email exists!";
            $response["code"] = 201;
            print($response);
        }
    }


}