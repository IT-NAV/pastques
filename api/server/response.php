<?php

/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 8/3/2016
 * Time: 1:50 PM
 */
class response
{
    private $data = [];

    function __construct() {
    }

    function getResults($array){
        $data = [];
        while($row = mysqli_fetch_assoc($array)){
            $data[] = $row;
        }
        return $data;
    }

    function fetchResult($array){
        $result = mysqli_fetch_assoc($array);
        return $result;
    }

    function simple_message($results,$msg1,$msg2){
        if($results){
            print($msg1);
        }else{
            print($msg2);
        }
    }

    function message_with_error($results,$msg,$conn){
        if($results){
            print($msg);
        }else{
            print($conn);
        }
    }
    

}