<?php

/**
 * Created by PhpStorm.
 * User: Che Henry
 * Date: 8/3/2016
 * Time: 1:50 PM
 */
class dbHandle
{
    private $results, $id;

    function __construct() {
    }

    function dependency(){
        require_once('response.php');
        return $res = new response();
    }

    function db_select($conn,$query){
        $res = $this->dependency();
        $result = mysqli_query($conn,$query);
        $ans = $res->getResults($result);
        return $ans;
    }

    function db_select_with_raw_result($conn,$query){
        $res = $this->dependency();
        $result = mysqli_query($conn,$query);
        $ans = $res->fetchResult($result);
        return $ans;
    }

    function db_insert_update_delete($conn, $query,$msg1,$msg2){
        $res = $this->dependency();
        $res->simple_message(mysqli_query($conn,$query),$msg1,$msg2);
    }

    function single_insert($conn,$query){
        $res = mysqli_query($conn,$query);
        print($res.mysqli_error($conn));
        return $res;

    }

    function get_insert_id($conn){
        return $this->id = mysqli_insert_id($conn);
    }


}